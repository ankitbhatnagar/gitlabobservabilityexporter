package gitlabobservabilityexporter

import (
	"context"

	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/exporter"
	"go.opentelemetry.io/collector/exporter/exporterhelper"
)

const (
	exporterType component.Type = "gitlabobservability"
)

func NewFactory() exporter.Factory {
	return exporter.NewFactory(
		exporterType,
		createDefaultConfig,
		exporter.WithTraces(createTracesExporter, component.StabilityLevelAlpha),
	)
}

func createDefaultConfig() component.Config {
	return &Config{}
}

func createTracesExporter(
	ctx context.Context,
	set exporter.CreateSettings,
	cfg component.Config,
) (exporter.Traces, error) {
	exp, err := newTracesExporter(set, cfg)
	if err != nil {
		return nil, err
	}
	return exporterhelper.NewTracesExporter(ctx, set, cfg, exp.extractErrors)
}
