package gitlabobservabilityexporter

import (
	"context"
	"fmt"

	"go.opentelemetry.io/collector/component"
	"go.opentelemetry.io/collector/exporter"
	"go.opentelemetry.io/collector/pdata/ptrace"
	"go.uber.org/zap"
)

type tracesExporter struct {
	cfg    *Config
	et     *ErrorTrackingClient
	logger *zap.Logger
}

func newTracesExporter(settings exporter.CreateSettings, c component.Config) (*tracesExporter, error) {
	cfg, ok := c.(*Config)
	if !ok {
		return nil, fmt.Errorf("invalid config: %#v", c)
	}

	et, err := NewErrorTrackingClient()
	if err != nil {
		return nil, fmt.Errorf("failed to build errortracking sender: %w", err)
	}

	return &tracesExporter{
		cfg:    cfg,
		et:     et,
		logger: settings.Logger,
	}, nil
}

func (e *tracesExporter) extractErrors(ctx context.Context, td ptrace.Traces) error {
	for i := 0; i < td.ResourceSpans().Len(); i++ {
		rspans := td.ResourceSpans().At(i)
		resource := rspans.Resource()
		parser := newParser(resource)
		for j := 0; j < rspans.ScopeSpans().Len(); j++ {
			ispans := rspans.ScopeSpans().At(j)
			for k := 0; k < ispans.Spans().Len(); k++ {
				select {
				case <-ctx.Done():
					return fmt.Errorf("context cancelled")
				default:
					parsed, err := parser.Parse(ispans.Spans().At(k))
					if err != nil {
						return err
					}
					if err := e.et.SendError(parsed); err != nil {
						return err
					}
				}
			}
		}
	}
	return nil
}
