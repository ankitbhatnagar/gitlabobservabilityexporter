package gitlabobservabilityexporter

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"time"
)

const (
	apiBaseURL   string = "https://observe.gitlab.com/errortracking/api/v1"
	projectID    int    = 37743989
	sentrySecret string = "MySentrySecret"
)

type ErrorTrackingClient struct {
	client *http.Client
}

func NewErrorTrackingClient() (*ErrorTrackingClient, error) {
	client := &http.Client{
		Timeout: 5 * time.Second,
	}
	return &ErrorTrackingClient{
		client: client,
	}, nil
}

func (e *ErrorTrackingClient) SendError(p IPayload) error {
	event, err := p.SentryCompatibleError()
	if err != nil {
		return err
	}

	payload, err := json.Marshal(event)
	if err != nil {
		return err
	}

	url := fmt.Sprintf("%s/projects/api/%d/store?sentry_key=%s",
		apiBaseURL,
		projectID,
		sentrySecret,
	)
	fmt.Printf("requesting: %s\n", url)

	req, err := http.NewRequest(http.MethodPost, url, bytes.NewBuffer(payload))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", fmt.Sprintf("Basic %s", base64.StdEncoding.EncodeToString([]byte(sentrySecret))))

	resp, err := e.client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	fmt.Println("response status:", resp.Status)
	fmt.Println("response headers:", resp.Header)
	body, _ := io.ReadAll(resp.Body)
	fmt.Println("response body:", string(body))

	return nil
}
