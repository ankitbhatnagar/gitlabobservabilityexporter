package gitlabobservabilityexporter

import (
	"crypto/rand"
	"errors"

	"github.com/getsentry/sentry-go"
	"github.com/google/uuid"
	"go.opentelemetry.io/collector/pdata/pcommon"
	"go.opentelemetry.io/collector/pdata/ptrace"
	conventions "go.opentelemetry.io/collector/semconv/v1.6.1"
)

var (
	errInvalidSpanID  = errors.New("SpanID is invalid")
	errInvalidTraceID = errors.New("TraceID is invalid")
)

type IPayload interface {
	SentryCompatibleError() (*sentry.Event, error)
}

type exception struct {
	exType    string
	exMessage string
}

type payload struct {
	Name         string
	TraceID      uuid.UUID
	SpanID       uuid.UUID
	ParentSpanID uuid.UUID
	Exceptions   []exception
}

func (p *payload) SentryCompatibleError() (*sentry.Event, error) {
	event := sentry.NewEvent()
	// build eventID
	b := make([]byte, 32)
	_, err := rand.Read(b)
	if err != nil {
		return nil, err
	}
	event.EventID = sentry.EventID(b)
	event.Contexts["trace"] = sentry.TraceContext{
		TraceID: sentry.TraceID(p.TraceID),
	}.Map()
	return event, nil
}

type parser struct {
	attributes pcommon.Map
}

func newParser(resource pcommon.Resource) *parser {
	return &parser{attributes: resource.Attributes()}
}

func (p *parser) Parse(span ptrace.Span) (*payload, error) {
	payload := &payload{
		Name: span.Name(),
	}

	traceID, err := traceIDtoUUID(span.TraceID())
	if err != nil {
		return nil, errInvalidTraceID
	}
	payload.TraceID = traceID

	spanID, err := spanIDtoUUID(span.SpanID())
	if err != nil {
		return nil, errInvalidSpanID
	}
	payload.SpanID = spanID

	payload.ParentSpanID = parentSpanIDtoUUID(span.ParentSpanID())

	payload.Exceptions = []exception{}
	for i := 0; i < span.Events().Len(); i++ {
		spanEvent := span.Events().At(i)
		if spanEvent.Name() != "exception" {
			continue // not an event we would be interested in
		}
		var (
			exceptionMessage string
			exceptionType    string
		)
		spanEvent.Attributes().Range(func(k string, v pcommon.Value) bool {
			switch k {
			case conventions.AttributeExceptionMessage:
				exceptionMessage = v.Str()
			case conventions.AttributeExceptionType:
				exceptionType = v.Str()
			}
			return true
		})
		if exceptionMessage == "" && exceptionType == "" {
			continue // atleast one should have been non-empty
		}
		payload.Exceptions = append(payload.Exceptions, exception{
			exType:    exceptionType,
			exMessage: exceptionMessage,
		})
	}

	return payload, nil
}

func traceIDtoUUID(id pcommon.TraceID) (uuid.UUID, error) {
	formatted, err := uuid.FromBytes(id[:])
	if err != nil || id.IsEmpty() {
		return uuid.Nil, errInvalidTraceID
	}
	return formatted, nil
}

func spanIDtoUUID(id pcommon.SpanID) (uuid.UUID, error) {
	formatted, err := uuid.FromBytes(padTo16Bytes(id))
	if err != nil || id.IsEmpty() {
		return uuid.Nil, errInvalidSpanID
	}
	return formatted, nil
}

func parentSpanIDtoUUID(id pcommon.SpanID) uuid.UUID {
	if id.IsEmpty() {
		return uuid.Nil
	}
	// FromBytes only returns an error if the length is not 16 bytes, so the error case is unreachable
	formatted, _ := uuid.FromBytes(padTo16Bytes(id))
	return formatted
}

func padTo16Bytes(b [8]byte) []byte {
	as16bytes := make([]byte, 16)
	copy(as16bytes[16-len(b):], b[:])
	return as16bytes
}
